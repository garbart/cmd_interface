#pragma once

#include <vector>
#include <string>
#include <map>

#include "command.h"


class cmd_interface
{
public:
    static void set_description(const std::string& value);
    static std::string& get_description();

    static void add_function(const command& cmd);

    static int run();
    static void stop();

    static std::vector<command> get_commands();
    static std::vector<command> get_commands(const std::string& category);

    static std::vector<std::string> get_categories();

private:
    static std::string description;
    static bool is_running;

    static std::map<std::string, command> commands_table;
    static std::map<std::string, std::vector<command>> categories_table;
};