#include "cmd_interface.h"
#include "common.h"


int main()
{
    cmd_interface::add_function(command({"help", "h"}, &common::help));
    cmd_interface::add_function(command({"quit", "q"}, &common::quit));
    return cmd_interface::run();
}
