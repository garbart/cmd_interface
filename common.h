#pragma once

#include "cmd_interface.h"


namespace common
{
    void help(std::vector<std::string>& args);

    void quit(std::vector<std::string>& args);
};
