#pragma once

#include <string>
#include <vector>


class command
{
public:
    explicit command(const std::vector<std::string>& names, const std::function<void(std::vector<std::string>&)>& function);
    command(const command& cmd);

    void operator()(std::vector<std::string>& args) const;

    void set_names(const std::vector<std::string>& names);
    const std::vector<std::string>& get_names() const;

    void set_description(const std::string& value);
    const std::string& get_description() const;

    void set_usage(const std::string& value);
    const std::string& get_usage() const;

    void set_category(const std::string &category);
    const std::string &get_category() const;

private:
    std::vector<std::string> names;
    std::function<void(std::vector<std::string>&)> function;
    std::string description;
    std::string usage;
    std::string category;
};
