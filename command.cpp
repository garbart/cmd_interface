#include "command.h"


using namespace std;


// public
command::command(const vector<string>& names, const ::function<void(vector<string>&)>& function)
{
    this->names = names;
    this->function = function;
    this->description = "";
    this->usage = "";
    this->category = "";
}
command::command(const command& cmd)
{
    this->names = cmd.names;
    this->function = cmd.function;
    this->description = cmd.description;
    this->usage = cmd.usage;
    this->category = cmd.category;
}

void command::operator()(vector<string>& args) const
{
    function(args);
}

void command::set_names(const vector<string> &value)
{
    names = value;
}
const vector<string> &command::get_names() const
{
    return names;
}

void command::set_description(const string& value)
{
    description = value;
}
const string& command::get_description() const
{
    return description;
}

void command::set_usage(const string& value)
{
    usage = value;
}
const string& command::get_usage() const
{
    return usage;
}

void command::set_category(const string &value)
{
    category = value;
}
const string &command::get_category() const
{
    return category;
}
