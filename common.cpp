#include <iostream>
#include <sstream>

#include "common.h"


using namespace std;


void common::help(std::vector<std::string>& args)
{
    for (const string& category : cmd_interface::get_categories())
    {
        for (const command& cmd : cmd_interface::get_commands(category))
        {
            stringstream names;
            for (const string& name : cmd.get_names())
            {
                names << name << " ";
            }
            const string& description = cmd.get_description();
            const string& usage = cmd.get_usage();
            cout << names.str() << " – " << endl;
            cout << "\tDescription – " << (description.empty() ? "no description info" : description) << endl;
            cout << "\tUsage – " << (usage.empty() ? "no usage info" : usage) << endl;
        }
        cout << endl;
    }
}

void common::quit(std::vector<std::string>& args)
{
    cmd_interface::stop();
}