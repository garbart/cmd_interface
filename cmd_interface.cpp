#include <iostream>

#include "cmd_interface.h"

using namespace std;


// public
void cmd_interface::set_description(const std::string& value)
{
    description = value;
}
std::string& cmd_interface::get_description()
{
    return description;
}

void cmd_interface::add_function(const command& cmd)
{
    for (const string& name : cmd.get_names())
    {
        commands_table.insert({name, cmd});
    }

    categories_table[cmd.get_category()].emplace_back(cmd);
}

int cmd_interface::run()
{
    is_running = true;
    while (is_running)
    {
        cout << "> ";
        cout.flush();

        bool was_quote = false;
        vector<string> args = {""};
        string line;
        getline(cin, line);
        for (char& ch : line)
        {
            if ((ch == ' ' and !was_quote) ||
                (ch == '"' and was_quote))
            {
                args.emplace_back("");
                was_quote = false;
            }
            else
            {
                if (ch == '"')
                {
                    was_quote = true;
                }
                else
                {
                    args.back().push_back(ch);
                }
            }
        }
        if (args.back().empty())
        {
            args.pop_back();
        }

        string cmd;
        if (!args.empty())
        {
            cmd = args.front();
            args.erase(args.begin());
        }

        if (cmd.empty())
        {
            continue;

        }
        else if (commands_table.find(cmd) == commands_table.end())
        {
            cout << "No such command \"" << cmd << "\"" << endl;
        }
        else
        {
            commands_table.at(cmd)(args);
        }
    }

    return 0;
}
void cmd_interface::stop()
{
    is_running = false;
}

vector<command> cmd_interface::get_commands()
{
    vector<command> output;
    output.reserve(commands_table.size());
    for (const auto& p : commands_table)
    {
        output.emplace_back(p.second);
    }
    return move(output);
}
vector<command> cmd_interface::get_commands(const string& category)
{
    vector<command> output = categories_table.at(category);
    return move(output);
}

vector<string> cmd_interface::get_categories()
{
    vector<string> output;
    output.reserve(categories_table.size());
    for (const auto& p : categories_table)
    {
        output.emplace_back(p.first);
    }
    return move(output);
}

// private
std::string cmd_interface::description;
bool cmd_interface::is_running = false;

std::map<std::string, command> cmd_interface::commands_table;
std::map<std::string, std::vector<command>> cmd_interface::categories_table;